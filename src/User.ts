import _ from 'lodash'
import { IRating } from './CollaborativeFiltering'
import { computeSimilarity } from './CosineSimilarity'
import { IRatings } from './UtilityMatrix'

export class User {
  private static _watched: IRatings = {}
  private static _hasNotWatched: {[userId: string]: string[]}

  private static _users: {[userId: string]: User} = {}

  private static _predictions: {
    [userId: string]: { movieId: string; prediction: number; }[]
  } = {}

  private static _ratings: {
    [userId: string]: {
      totalRated: number,
      totalRating: number
    }
  } = {}

  private static _similarity: {
    [userId: string]: {
      [userId: string]: number
    }
  } = {}

  private userId: string

  static get similarity() {
    return User._similarity
  }

  static loadRatings(ratings: IRating[]) {
    for (let i = 0, l = ratings.length; i < l; i ++) {
      const [userId, movieId, rating] = ratings[i]
      User.getUser(userId).setRating(movieId, rating)
    }
  }

  static loadWatched(ratings: IRatings, movies: string[]) {
    this._watched = ratings
    this._hasNotWatched = Object.entries(ratings).reduce((res, cur) => {
      res[cur[0]] = _.difference(movies, Object.keys(cur[1]));
      
      return res
    }, {} as typeof User._hasNotWatched)
  }

  static loadSimilarity(similarity: typeof User._similarity) {
    User._similarity = similarity
  }

  static getUser(userId: string): User {
    if (!User._users[userId])
      User._users[userId] = new User(userId)
 
    return User._users[userId]
  }

  static getAverageRatings(): {[userId: string]: number} {
    return Object.values(User._users).reduce((res, cur) => {
      res[cur.userId] = cur.getAverageRating()
      return res
    }, {} as {[userId: string]: number})
  }

  private constructor (userId: string) {
    this.userId = userId
    if (!User._ratings[this.userId]) this.initialUser()
  }

  private initialUser() {
    User._watched[this.userId] = {}
    User._ratings[this.userId] = {
      totalRated: 0,
      totalRating: 0
    }
  }

  get watched(): string[] {
    return Object.keys(
      User._watched[this.userId] ?? {}
    )
  }

  get hasNotWatched() {
    return User._hasNotWatched[this.userId]
  }

  hasWatched(movieId: string): boolean {
    return _.isNumber(this.getRating(movieId))
  }

  getRating(movieId: string): number {
    return User._watched[this.userId][movieId]
  }

  getAverageRating(): number {
    const { totalRated, totalRating } = User._ratings[this.userId]
    return totalRating / totalRated
  }

  setRating(movieId: string, rating: number) {
    const { totalRated, totalRating } = User._ratings[this.userId]

    User._watched[this.userId][movieId] = rating

    User._ratings[this.userId] = {
      totalRated: totalRated + 1,
      totalRating: totalRating + rating
    }
  }

  setSimilarity(userId: string, ratings: IRatings) {
    const r1 = _.values(ratings[userId]) 
    const r2 = _.values(ratings[this.userId])

    const sim = computeSimilarity(r1, r2)

    if (!User._similarity[this.userId])
      User._similarity[this.userId] = {}

    if (!User._similarity[userId])
      User._similarity[userId] = {}

    User._similarity[this.userId][userId] = sim
    User._similarity[userId][this.userId] = sim
  }

  getTopSimilarity() {
    return _.entries(User._similarity[this.userId])
      .sort((e1, e2) => e2[1] - e1[1])
      .map(e => ({ userId: e[0], similarity: e[1] }))
  }

  getTopSimilarityWatchedMovie(
    movieId: string,
    top: number = 10
  ) {
    return this.getTopSimilarity()
      .filter(e => User.getUser(e.userId).hasWatched(movieId))
      .map(e => ({ ...e, rating: User.getUser(e.userId).getRating(movieId)}))
      .slice(0, top)
  }

  predict() {
    const result: {[movieId: string]: number} = {}

    if (!User._predictions[this.userId]) {
      for (let i = 0, l = this.hasNotWatched.length; i < l; i ++) {
        let top = 0
        let btm = 0
        const movieId = this.hasNotWatched[i]
  
        this.getTopSimilarityWatchedMovie(movieId).forEach(e => {
          top += e.rating * e.similarity
          btm += Math.abs(e.similarity)
        })
  
        const val = top / btm
  
        result[movieId] = _.isNaN(val) ? 0 : val
      }

      User._predictions[this.userId] = Object.entries(result)
        .sort((e1, e2) => e2[1] - e1[1])
        .map(e => ({ movieId: e[0], prediction: e[1] }))
    }

    return User._predictions[this.userId]
  }
}