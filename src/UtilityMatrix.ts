import { IRating } from './CollaborativeFiltering'

export interface IRatings {
  [userId: string]: {
    [movieId: string]: number
  }
}

export class UtilityMatrix {
  private _users: string[]
  private _movies: string[]
  private _originalRatings: IRatings = {}
  private _normalizedRatings: IRatings = {}

  constructor (ratings: IRating[]) {
    this._initialMatrix(ratings)
  }

  get users() {
    return this._users
  }

  get movies() {
    return this._movies
  }

  get originalRatings() {
    return this._originalRatings
  }

  get normalizedRatings() {
    return this._normalizedRatings
  }

  private _initialMatrix (ratings: IRating[]) {
    let users: {[userId: string]: boolean} = {}
    let movies: {[movieId: string]: boolean} = {}

    for (let i = 0, l = ratings.length; i < l; i ++) {
      const [userId, movieId, rating] = ratings[i]

      if (!this._originalRatings[userId])
        this._originalRatings[userId] = {}

      this._originalRatings[userId][movieId] = rating
      movies[movieId] = true
      users[userId] = true
    }

    this._users = Object.keys(users)
    this._movies = Object.keys(movies)
  }

  normalizeRatings(avgRatings: {[userId: string]: number}) {
    for (let i = 0, mL = this._users.length; i < mL; i ++) {
      const userId = this._users[i]
      this._normalizedRatings[userId] = {}

      for (let j = 0, uL = this._movies.length; j < uL; j ++) {
        const movieId = this._movies[j]

        if (this._originalRatings[userId][movieId]) {
          this._normalizedRatings[userId][movieId] =
            this._originalRatings[userId][movieId] - avgRatings[userId]
        } else {
          this._normalizedRatings[userId][movieId] = 0
        }

        this._originalRatings[userId][movieId]
      }
    }
  }
}