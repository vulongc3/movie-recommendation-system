const similarity = require('compute-cosine-similarity')

export const computeSimilarity = (a1: number[], a2: number[]): number => {
  return similarity(a1, a2)
}