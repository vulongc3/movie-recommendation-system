import _ from 'lodash'
import { readFileSync, writeFileSync } from 'fs'
import { resolve } from 'path'
import { User } from './User'
import { UtilityMatrix } from './UtilityMatrix'

export declare type IRating = [string, string, number]

export class CollaborativeFiltering {
  private _utilityMatrix: UtilityMatrix

  constructor (ratings: IRating[], isFromFile: boolean = false) {
    this._utilityMatrix = new UtilityMatrix(ratings)

    User.loadRatings(ratings)
    User.loadWatched(
      this._utilityMatrix.originalRatings,
      this._utilityMatrix.movies
    )

    this._utilityMatrix.normalizeRatings(User.getAverageRatings())
    if (isFromFile) this._computeSimilaritiesFromFile()
    else this._computeSimilarities()
  }

  private _computeSimilarities() {
    for (let i = 0, l = this._utilityMatrix.users.length; i < l; i ++) {
      const u1 = this._utilityMatrix.users[i]

      for (let j = 0; j < l; j ++) {
        const u2 = this._utilityMatrix.users[j]

        User.getUser(u1).setSimilarity(
          u2,
          this._utilityMatrix.normalizedRatings
        )
      }
    }

    writeFileSync('cache/sm.json', JSON.stringify(User.similarity))
  }

  private _computeSimilaritiesFromFile() {
    const path = resolve(__dirname, '../cache/sm.json')
    const sm = readFileSync(path, { encoding: 'utf-8' })
    User.loadSimilarity(JSON.parse(sm))

  }
  
  get utilityMatrix() {
    return this._utilityMatrix
  }
  

  predict(userId: string, top: number = 10) {
    return User.getUser(userId).predict().slice(0, top)
  }
}