import _ from 'lodash'
import cliSelect from 'cli-select'
import { parseFile } from 'fast-csv'
import { IRating, CollaborativeFiltering } from './CollaborativeFiltering'
import { writeFileSync } from 'fs'
import { User } from './User'
import { exec } from 'child_process'

const selects = [ 'From scratch', 'From file' ]

const main = () => {
  const rows: IRating[] = []

  cliSelect({ values: selects }).then(res => {
    parseFile('data/ratings.csv').on('data', row => {
      const [ userId, movieId, rating ] = row
      rows.push([ userId, movieId, Number(rating) ])
    }).once('end', () => {
      const _rows = _.shuffle(rows.slice(1))
      const trainData = _.sampleSize(_rows, 10000)

      const cf = new CollaborativeFiltering(
        trainData,
        res.value === selects[1]
      )

      const predictions = _.sampleSize(cf.utilityMatrix.users, 10).reduce(
        (res, userId) => {
          res[userId] = User.getUser(userId).predict()
          return res
        },
        {} as {[userId: string]: any}
      )
  
      writeFileSync('outputs/predictions.json', JSON.stringify(predictions))
      console.log('Saved predictions')
      exec('nautilus ./outputs')
    })
  })
}

main()